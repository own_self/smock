package com.smock.models;

import java.util.List;

public class ValidationContextDTO {

    private int validationContextId;
    private int apiId;
    private ValidationType validationType;
    private List<ValidationEntity> validationEntities;

    public int getValidationContextId() {
        return validationContextId;
    }

    public void setValidationContextId(int validationContextId) {
        this.validationContextId = validationContextId;
    }

    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public void setValidationType(ValidationType validationType) {
        this.validationType = validationType;
    }

    public List<ValidationEntity> getValidationEntities() {
        return validationEntities;
    }

    public void setValidationEntities(List<ValidationEntity> validationEntities) {
        this.validationEntities = validationEntities;
    }
}
