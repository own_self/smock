package com.smock.services;


import com.smock.config.ApiConfig;
import com.smock.models.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class ApiRequestHandler {

    private static final Logger LOGGER = LogManager.getLogger(ApiRequestHandler.class);

    @Autowired
    private UrlPerformanceFactory urlPerformanceFactory;
    @Autowired
    private ActionAdvisor actionAdvisor;

    public ApiResponse handleApiRequest(HttpServletRequest request) {

        long stepStart = System.currentTimeMillis();

        ApiInfo apiInfo = getApiInfo(request);
        if (apiInfo == null) {
            return null;
        }
        Action action = actionAdvisor.getAction(apiInfo);

        long actionAdviseAbsTime = System.currentTimeMillis();

        long actionAdviseTime = actionAdviseAbsTime - stepStart;

        if (action.getLatency() > 1000) {
            LOGGER.info("higher....");
        }

        long finalLatency = action.getLatency() - actionAdviseTime;

        if (finalLatency < 0) {
            finalLatency = 0;
        }

        try {
            Thread.sleep(finalLatency);
        } catch (InterruptedException e) {
            LOGGER.warn("Thread sleep interrupted: ", e);
        }

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatusCode(action.getStatusCode());
        apiResponse.setBody(action.getResponse());

        return apiResponse;

    }

    //TODO: publish event and log
    private void handleLatencyBreach(ApiInfo apiInfo, long actualLatency) {

    }

    public ApiInfo getApiInfo(HttpServletRequest request) {
        String url = request.getRequestURI();
        String method = request.getMethod();
        ApiConfig apiConfig = urlPerformanceFactory.getApiConfig(url, method);
        if (apiConfig == null) {
            return null;
        }
        return new ApiInfo(apiConfig.getId(), apiConfig.getName(), url, apiConfig.getPattern(), ApiMethod.valueOf(method.toUpperCase()));
    }

}
