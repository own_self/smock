package com.smock.cron;


import com.smock.config.ApiConfig;
import com.smock.config.MisbehaveConfig;
import com.smock.config.PerformanceConfig;
import com.smock.models.ApiInfo;
import com.smock.models.ApiStatus;
import com.smock.services.ActionAdvisor;
import com.smock.services.UrlPerformanceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Service
public class BehaviourCron {

    private static final Logger LOGGER = LogManager.getLogger(BehaviourCron.class);

    @Autowired private PerformanceConfig performanceConfig;
    @Autowired private ActionAdvisor actionAdvisor;

    public BehaviourCron() {
    }

    @Scheduled(fixedRate = 1000)
    public void behaviourCron() {
        List<ApiConfig> apiConfigs = performanceConfig.getApis();
        for (ApiConfig apiConfig : apiConfigs) {
            ApiInfo apiInfo = new ApiInfo(apiConfig.getId(), apiConfig.getName(), apiConfig.getPattern(), apiConfig.getPattern(), apiConfig.getApiMethod());
            ApiStatus apiStatus = actionAdvisor.getApiStatus(apiInfo);
            if (apiStatus == null) {
                apiStatus = new ApiStatus(apiConfig.getId());
                apiStatus.setLatencyMisbehaving(false);
            }
            if (apiStatus.isLatencyMisbehaving()) {
                long latencyMisbehaveStartTimestamp = apiStatus.getLatencyMisbehaveStartTimestamp();
                long curr = System.currentTimeMillis();

                int latencyDuration = apiConfig.getMisbehave().getLatencyDuration();

                if (curr - latencyMisbehaveStartTimestamp > latencyDuration) {
                    apiStatus.setLatencyMisbehaving(false);
                }
            } else {
                double random = Math.random();
//                LOGGER.info(random + " : " + apiConfig.getMisbehave().getLatencyProbability());
                if (random < apiConfig.getMisbehave().getLatencyProbability()) {
                    apiStatus.setLatencyMisbehaving(true);
                    apiStatus.setLatencyMisbehaveStartTimestamp(System.currentTimeMillis());
                }
            }

        }
    }

}
