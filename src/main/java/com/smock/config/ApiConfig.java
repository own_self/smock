package com.smock.config;


import com.smock.models.ApiMethod;

public class ApiConfig {

    private int id;
    private String name;
    private String pattern;
    private ApiMethod apiMethod;
    private int latency;
    private int statusCode;

    private ResponseConfig response;
    private MisbehaveConfig misbehave;

    public ApiConfig() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public ApiMethod getApiMethod() {
        return apiMethod;
    }

    public void setApiMethod(ApiMethod apiMethod) {
        this.apiMethod = apiMethod;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public MisbehaveConfig getMisbehave() {
        return misbehave;
    }

    public void setMisbehave(MisbehaveConfig misbehave) {
        this.misbehave = misbehave;
    }

    public ResponseConfig getResponse() {
        return response;
    }

    public void setResponse(ResponseConfig response) {
        this.response = response;
    }
}
