package com.smock.models;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ApiStatus {

    private int id;
    private Map<Integer, StatusStats> statusStatsMap;
    private boolean isLatencyMisbehaving;
    private int latencyMisbehaveCount;
    private long latencyMisbehaveStartTimestamp;
    private int currStatus;
    private long currStatusStartTimestamp;


    public ApiStatus() {
        statusStatsMap = new ConcurrentHashMap<>();
        currStatus = 200;
        latencyMisbehaveCount = 0;
        currStatusStartTimestamp = System.currentTimeMillis();
        isLatencyMisbehaving = false;
        latencyMisbehaveStartTimestamp = System.currentTimeMillis();
    }

    public ApiStatus(int id) {
        this();
        this.id = id;
    }

    public ApiStatus(int id, Map<Integer, StatusStats> statusStatsMap, int currStatus, long currStatusStartTimestamp) {
        this.id = id;
        this.statusStatsMap = statusStatsMap;
        this.currStatus = currStatus;
        this.currStatusStartTimestamp = currStatusStartTimestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<Integer, StatusStats> getStatusStatsMap() {
        return statusStatsMap;
    }

    public void setStatusStatsMap(Map<Integer, StatusStats> statusStatsMap) {
        this.statusStatsMap = statusStatsMap;
    }

    public int getCurrStatus() {
        return currStatus;
    }

    public void setCurrStatus(int currStatus) {
        this.currStatus = currStatus;
    }

    public long getCurrStatusStartTimestamp() {
        return currStatusStartTimestamp;
    }

    public void setCurrStatusStartTimestamp(long currStatusStartTimestamp) {
        this.currStatusStartTimestamp = currStatusStartTimestamp;
    }

    public boolean isLatencyMisbehaving() {
        return isLatencyMisbehaving;
    }

    public void setLatencyMisbehaving(boolean latencyMisbehaving) {
        isLatencyMisbehaving = latencyMisbehaving;
    }

    public long getLatencyMisbehaveStartTimestamp() {
        return latencyMisbehaveStartTimestamp;
    }

    public void setLatencyMisbehaveStartTimestamp(long latencyMisbehaveStartTimestamp) {
        this.latencyMisbehaveStartTimestamp = latencyMisbehaveStartTimestamp;
    }

    public int getLatencyMisbehaveCount() {
        return latencyMisbehaveCount;
    }

    public void setLatencyMisbehaveCount(int latencyMisbehaveCount) {
        this.latencyMisbehaveCount = latencyMisbehaveCount;
    }
}
