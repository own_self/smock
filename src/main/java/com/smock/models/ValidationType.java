package com.smock.models;

public enum ValidationType {

    URL_MATCHER, HEADER_MATCHER, BODY_MATCHER;

}
