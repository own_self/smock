package com.smock.controllers;


import com.chaosimC.chaoscommons.models.MetricTagValue;
import com.chaosimC.chaoscommons.services.MetricUtils;
import com.smock.config.Constants;
import com.smock.config.ServiceConfig;
import com.smock.models.ApiInfo;
import com.smock.models.ApiResponse;
import com.smock.models.ValidationType;
import com.smock.services.ApiRequestHandler;
import com.smock.services.ValidationContextHolder;
import com.smock.services.ValidationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/mocked")
public class GenericController {

    private static final Logger LOGGER = LogManager.getLogger(GenericController.class);

    @Autowired
    private ApiRequestHandler apiRequestHandler;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ValidationContextHolder validationContextHolder;

    @Autowired
    private ServiceConfig serviceConfig;

    @ResponseBody
    @RequestMapping("/**")
    public ResponseEntity<String> generic(HttpServletRequest request, @RequestBody(required = false) String body) {
        long tic = System.currentTimeMillis();
        ApiResponse apiResponse = apiRequestHandler.handleApiRequest(request);
        if (apiResponse == null) {
            LOGGER.warn("Could not find API " + request.getRequestURL());
            return ResponseEntity.ok("");
        }
        long toc = System.currentTimeMillis();

        ApiInfo apiInfo = apiRequestHandler.getApiInfo(request);
        if(apiInfo != null && serviceConfig.isValidationModeOn()) {
            updateObservedValidations(request, apiInfo.getId(), body);
        }

        List<MetricTagValue> metricTagValues = new ArrayList<>();
        metricTagValues.add(new MetricTagValue("api", apiInfo.getUrlPattern()));
        metricTagValues.add(new MetricTagValue("method", apiInfo.getApiMethod().toString()));
        MetricUtils.recordHistogram(Constants.COMPONENT, Constants.METRIC_HIST, (toc - tic), metricTagValues);
        return new ResponseEntity<>(apiResponse.getBody(), HttpStatus.resolve(apiResponse.getStatusCode()));
    }

    private void updateObservedValidations( HttpServletRequest request, int apiId, String body) {
        if(body == null) {
            return;
        }
        String url = request.getRequestURI();
        url = url.replaceFirst("/mocked", "");
        Enumeration<String> headerNames = request.getHeaderNames();
        Map<String, Object> headersValidationMap = new HashMap<>();
        while(headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            String headerValue = request.getHeader(header);
            headersValidationMap.put(header, headerValue);
        }
        Map<String, Object> urlValidationMap = new HashMap<>();
        urlValidationMap.put("URL", url);

        Map<String, Object> bodyValidationMap = new HashMap<>();
        body = body.replaceAll("[\\n\\t ]", "");
        bodyValidationMap.put("BODY", body);

        validationService.updateObserved(apiId, ValidationType.URL_MATCHER, urlValidationMap);
        validationService.updateObserved(apiId, ValidationType.HEADER_MATCHER, headersValidationMap);
        validationService.updateObserved(apiId, ValidationType.BODY_MATCHER, bodyValidationMap);
    }

}
