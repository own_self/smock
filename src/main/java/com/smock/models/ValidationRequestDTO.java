package com.smock.models;

import java.util.List;

public class ValidationRequestDTO {

    private List<ValidationType> validationTypes;

    public List<ValidationType> getValidationTypes() {
        return validationTypes;
    }

    public void setValidationTypes(List<ValidationType> validationTypes) {
        this.validationTypes = validationTypes;
    }
}
