package com.smock.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("performance")
public class PerformanceConfig {

    private List<ApiConfig> apis;

    public PerformanceConfig() {
    }

    public List<ApiConfig> getApis() {
        return apis;
    }

    public void setApis(List<ApiConfig> apis) {
        this.apis = apis;
    }
}
