package com.smock.models;

public enum ApiMethod {

    GET, POST, PUT, DELETE;

}
