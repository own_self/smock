package com.smock.models;

import java.util.concurrent.atomic.AtomicInteger;

public class StatusStats {

    private AtomicInteger totalCount;

    public StatusStats() {
        totalCount = new AtomicInteger(0);
    }

    public StatusStats(AtomicInteger totalCount) {
        this.totalCount = totalCount;
    }

    public AtomicInteger getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(AtomicInteger totalCount) {
        this.totalCount = totalCount;
    }

}
