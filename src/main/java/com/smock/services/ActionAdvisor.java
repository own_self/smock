package com.smock.services;

import com.smock.config.ApiConfig;
import com.smock.models.Action;
import com.smock.models.ApiInfo;
import com.smock.models.ApiStatus;
import com.smock.models.BehaviourInterrupt;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ActionAdvisor {

    private static final Logger LOGGER = LogManager.getLogger(ActionAdvisor.class);

    @Autowired
    private UrlPerformanceFactory urlPerformanceFactory;

    @Autowired
    private BehaviourInterruptManager behaviourInterruptManager;

    @Autowired
    private ResponseFactory responseFactory;

    @Autowired
    private CustomResponseMapper customResponseMapper;

    private Map<ApiInfo, ApiStatus> apiStatusMap;


    public ActionAdvisor() {
        apiStatusMap = new ConcurrentHashMap<>();
    }

    public Action getAction(ApiInfo apiInfo) {
        Action action = new Action();
        action.setStatusCode(200);
        action.setLatency(getLatency(apiInfo));
//        action.setResponse(urlPerformanceFactory.getApiConfig(apiInfo).getResponse().getStr());
        String response;
        if(customResponseMapper.getCustomResponseConfig(apiInfo.getId()) != null) {
            Integer statusCode = customResponseMapper.getCustomResponseConfig(apiInfo.getId()).getStatusCode();
            if(statusCode != null) {
                action.setStatusCode(statusCode);
            }
            Integer latency = customResponseMapper.getCustomResponseConfig(apiInfo.getId()).getLatency();
            if(latency != null) {
                action.setLatency(latency);
            }
            Integer responseId = customResponseMapper.getCustomResponseConfig(apiInfo.getId()).getResponseId();
            if(responseId != null) {
                response = responseFactory.getResponse(apiInfo, responseId);
            } else {
                response = responseFactory.getResponse(apiInfo);
            }
        } else {
            response = responseFactory.getResponse(apiInfo);
        }
        action.setResponse(response);
        return action;
    }

    private int getLatency(ApiInfo apiInfo) {
        Optional<BehaviourInterrupt> behaviourInterruptOptional = behaviourInterruptManager.getBehaviourInterrupt(apiInfo.getId());
        if (behaviourInterruptOptional.isPresent()) {
            BehaviourInterrupt behaviourInterrupt = behaviourInterruptOptional.get();
            if ((System.currentTimeMillis() - behaviourInterrupt.getStartTime()) / 1000 > behaviourInterrupt.getDuration()) {
                behaviourInterruptManager.expire(apiInfo.getId());
            } else {
                return behaviourInterrupt.getLatency();
            }
        }
        ApiConfig apiConfig = urlPerformanceFactory.getApiConfig(apiInfo);
        ApiStatus apiStatus = apiStatusMap.get(apiInfo);
        if (apiStatus == null) {
            apiStatus = new ApiStatus();
            apiStatus.setLatencyMisbehaving(false);
            apiStatus.setLatencyMisbehaveCount(0);
            apiStatusMap.put(apiInfo, apiStatus);
        }

        int latency = 0;

        if (apiStatus.isLatencyMisbehaving()) {
            int low = apiConfig.getMisbehave().getLatencyLow();
            int high = apiConfig.getMisbehave().getLatencyHigh();
            int range = high - low;
            Random random = new Random();
            latency = low + random.nextInt(range);
        } else {
            latency = apiConfig.getLatency();
        }

        return latency;

    }

    public ApiStatus getApiStatus(ApiInfo apiInfo) {
        return apiStatusMap.get(apiInfo);
    }


}
