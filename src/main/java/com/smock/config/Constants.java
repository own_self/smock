package com.smock.config;

public class Constants {

    public static final String COMPONENT = "smock";

    public static final String METRIC_COUNT = "smock_count";
    public static final String METRIC_HIST = "smock_hist";

}
