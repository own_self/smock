package com.smock.models;

import java.util.List;

public class ValidationResponse {

    private int validationContextId;
    private int apiId;
    private boolean isValid = true;
    private List<ValidationFailureEntity> validationFailureEntities;

    public int getValidationContextId() {
        return validationContextId;
    }

    public void setValidationContextId(int validationContextId) {
        this.validationContextId = validationContextId;
    }

    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public List<ValidationFailureEntity> getValidationFailureEntities() {
        return validationFailureEntities;
    }

    public void setValidationFailureEntities(List<ValidationFailureEntity> validationFailureEntities) {
        this.validationFailureEntities = validationFailureEntities;
    }
}
