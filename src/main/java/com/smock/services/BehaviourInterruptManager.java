package com.smock.services;

import com.smock.models.BehaviourInterrupt;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Service
public class BehaviourInterruptManager {

    private Map<Integer, BehaviourInterrupt> apiBehaviourInterruptMap = new HashMap<>();

    public Optional<BehaviourInterrupt> getBehaviourInterrupt(int apiId) {
        return apiBehaviourInterruptMap.containsKey(apiId) ? Optional.of(apiBehaviourInterruptMap.get(apiId)) : Optional.empty();
    }

    public void interrupt(int apiId, int latency, long duration) {
        BehaviourInterrupt behaviourInterrupt = new BehaviourInterrupt();
        behaviourInterrupt.setLatency(latency);
        behaviourInterrupt.setDuration(duration);
        apiBehaviourInterruptMap.put(apiId, behaviourInterrupt);
    }

    public void expire(int apiId) {
        apiBehaviourInterruptMap.remove(apiId);
    }

}
