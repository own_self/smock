package com.smock.models;

import java.util.Objects;

public class ResponsePath {

    private int id;
    private String path;
    private boolean defaultPath;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isDefaultPath() {
        return defaultPath;
    }

    public void setDefaultPath(boolean defaultPath) {
        this.defaultPath = defaultPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponsePath that = (ResponsePath) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
