package com.smock.config;

public class StartupConfig {

    public static int testSuiteId;
    public static int lifetime;
    public static long suiteStartTime;

    public static void init (int testSuiteId, int lifetime, long suiteStartTime) {
        StartupConfig.testSuiteId = testSuiteId;
        StartupConfig.lifetime = lifetime;
        StartupConfig.suiteStartTime = suiteStartTime;
    }

}
