package com.smock.models;

import java.util.Objects;

public class ApiRequest {

    private String url;
    private ApiMethod method;

    public ApiRequest() {
    }

    public ApiRequest(String url, ApiMethod method) {
        this.url = url;
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ApiMethod getMethod() {
        return method;
    }

    public void setMethod(ApiMethod method) {
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiRequest that = (ApiRequest) o;
        return Objects.equals(url, that.url) &&
                method == that.method;
    }

    @Override
    public int hashCode() {

        return Objects.hash(url, method);
    }
}
