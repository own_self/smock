package com.smock.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEventListenerService implements ApplicationListener<ApplicationStartedEvent> {

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {

        int testSuiteId;
        int lifetime;
        long suiteStartTime;

        String testSuiteIdProperty = System.getProperty("testSuiteId");
        if (testSuiteIdProperty == null) {
            testSuiteId = 1;
        } else {
            try {
                testSuiteId = Integer.parseInt(testSuiteIdProperty);
            } catch (NumberFormatException e) {
                testSuiteId = 1;
            }
        }

        String lifetimeProperty = System.getProperty("lifetime");
        if (lifetimeProperty == null) {
            lifetime = 10 * 60;
        } else {
            try {
                lifetime = Integer.parseInt(lifetimeProperty);
            } catch (NumberFormatException e) {
                lifetime = 10 * 60;
            }
        }

        String suiteStartTimeProperty = System.getProperty("suiteStartTime");
        if (suiteStartTimeProperty == null) {
            suiteStartTime = System.currentTimeMillis();
        } else {
            try {
                suiteStartTime = Long.parseLong(suiteStartTimeProperty);
            } catch (NumberFormatException e) {
                suiteStartTime = System.currentTimeMillis();
            }
        }

        StartupConfig.init(testSuiteId, lifetime, suiteStartTime);

    }
}
