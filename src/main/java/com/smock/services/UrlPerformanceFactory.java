package com.smock.services;


import com.smock.config.PerformanceConfig;
import com.smock.config.ApiConfig;
import com.smock.models.ApiInfo;
import com.smock.models.ApiMethod;
import com.smock.models.ApiRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UrlPerformanceFactory {

    @Autowired
    private PerformanceConfig performanceConfig;

    private static final Logger LOGGER = LogManager.getLogger(UrlPerformanceFactory.class);

    private Map<Integer, ApiConfig> idUrlConfigMap;
    private Map<ApiRequest, ApiConfig> urlUrlConfigMap;

    @PostConstruct
    private void init() {
        idUrlConfigMap = new HashMap<>();
        urlUrlConfigMap = new HashMap<>();
        List<ApiConfig> apiConfigList = performanceConfig.getApis();
        apiConfigList.forEach(apiConfig -> {
            LOGGER.info("Initializing api Id: " + apiConfig.getId() + " with name: " + apiConfig.getName()
             + " and pattern: " + apiConfig.getPattern());
            idUrlConfigMap.put(apiConfig.getId(), apiConfig);
            urlUrlConfigMap.put(new ApiRequest(apiConfig.getPattern(), apiConfig.getApiMethod()), apiConfig);
        });

    }

    public ApiConfig getApiConfig(int id) {
        return idUrlConfigMap.get(id);
    }

    public ApiConfig getApiConfig(String url, String apiMethod) {
//        return urlUrlConfigMap.get(new ApiRequest(url, ApiMethod.valueOf(apiMethod.toUpperCase())));
        for (ApiRequest apiRequest : urlUrlConfigMap.keySet()) {
            if (apiRequest.getMethod().toString().toLowerCase().equals(apiMethod.toLowerCase())) {
                Pattern pattern = Pattern.compile(apiRequest.getUrl());
                Matcher matcher = pattern.matcher(url);
                if (matcher.matches()) {
                    return urlUrlConfigMap.get(apiRequest);
                }
            }
        }
        return null;
    }

    public ApiConfig getApiConfig(ApiInfo apiInfo) {
        return this.getApiConfig(apiInfo.getId());
    }

}
