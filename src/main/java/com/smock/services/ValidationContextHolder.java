package com.smock.services;

import com.smock.models.ValidationContext;
import com.smock.models.ValidationType;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class ValidationContextHolder {

    private CacheManager cacheManager;
    private Cache<Integer, ValidationContext> expectedCache;
    private Cache<Integer, ValidationContext> observedCache;

    @PostConstruct
    public void init() {
        cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build();
        cacheManager.init();
        CacheConfigurationBuilder<Integer, ValidationContext> expectedConfigurationBuilder =
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Integer.class, ValidationContext.class, ResourcePoolsBuilder.heap(1000))
                        .withExpiry(Expirations.timeToLiveExpiration(new Duration(60, TimeUnit.SECONDS)));
        expectedCache = cacheManager.createCache("VALIDATION_ENTITIES_CACHE", expectedConfigurationBuilder);

        CacheConfigurationBuilder<Integer, ValidationContext> observedConfigurationBuilder =
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Integer.class, ValidationContext.class, ResourcePoolsBuilder.heap(1000))
                            .withExpiry(Expirations.timeToLiveExpiration(new Duration(90, TimeUnit.SECONDS)));
        observedCache = cacheManager.createCache("OBSERVED_VALIDATION_ENTITIES_CACHE",observedConfigurationBuilder);
    }

    public void addExpectedContext(int validationContextId, ValidationContext validationContext) {
        expectedCache.put(validationContextId, validationContext);
    }

    public Optional<ValidationContext> getValidationContext(int validationContextId) {
        return expectedCache.get(validationContextId) == null ? Optional.empty() : Optional.of(expectedCache.get(validationContextId));
    }

    public Optional<Map<ValidationType, Map<String, Object>>> getValidationContextForApiId(int validationContextId, int apiId) {
        ValidationContext validationContext = expectedCache.get(validationContextId);
        if(validationContext != null) {
            Map<ValidationType, Map<String, Object>> validationTypeMapMap = validationContext.getContext().get(apiId);
            if(validationTypeMapMap != null && !validationTypeMapMap.isEmpty()) {
                return Optional.of(validationTypeMapMap);
            }
        }
        return Optional.empty();
    }

    public Optional<ValidationContext> getObservedContext(int apiId) {
        ValidationContext validationContext = observedCache.get(apiId);
        return validationContext == null ? Optional.empty() : Optional.of(validationContext);
    }

    public void removeObservedContext(int apiId) {
        observedCache.remove(apiId);
    }

    public void addObservedContext(int apiId, ValidationContext validationContext) {
        observedCache.put(apiId, validationContext);
    }

}
