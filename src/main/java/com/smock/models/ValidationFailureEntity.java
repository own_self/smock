package com.smock.models;

public class ValidationFailureEntity {

    private ValidationType validationType;
    private String key;
    private Object expectedValue;
    private Object observedValue;

    public ValidationType getValidationType() {
        return validationType;
    }

    public void setValidationType(ValidationType validationType) {
        this.validationType = validationType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(Object expectedValue) {
        this.expectedValue = expectedValue;
    }

    public Object getObservedValue() {
        return observedValue;
    }

    public void setObservedValue(Object observedValue) {
        this.observedValue = observedValue;
    }
}
