package com.smock.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("service")
public class ServiceConfig {

    private boolean validationModeOn;

    public boolean isValidationModeOn() {
        return validationModeOn;
    }

    public void setValidationModeOn(boolean validationModeOn) {
        this.validationModeOn = validationModeOn;
    }
}
