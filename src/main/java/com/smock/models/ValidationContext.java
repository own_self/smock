package com.smock.models;

import java.util.HashMap;
import java.util.Map;

public class ValidationContext {

    private Map<Integer, Map<ValidationType, Map<String, Object>>> context = new HashMap<>();
    private Map<ValidationType, Map<String, Object>> observedContext = new HashMap<>();

    public Map<Integer, Map<ValidationType, Map<String, Object>>> getContext() {
        return context;
    }

    public void setContext(Map<Integer, Map<ValidationType, Map<String, Object>>> context) {
        this.context = context;
    }

    public Map<ValidationType, Map<String, Object>> getObservedContext() {
        return observedContext;
    }

    public void setObservedContext(Map<ValidationType, Map<String, Object>> observedContext) {
        this.observedContext = observedContext;
    }
}
