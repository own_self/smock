package com.smock.config;

import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;

public class StatsdConfig {

    public static final StatsDClient statsd = new NonBlockingStatsDClient(
            null,                          /* prefix to any stats; may be null or empty string */
            "localhost",                        /* common case: localhost */
            8125,/* port */
            new String[] {"component:smock"}/* Datadog extension: Constant tags, always applied */
    );

}
