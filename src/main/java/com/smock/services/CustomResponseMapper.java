package com.smock.services;


import com.smock.config.ApiConfig;
import com.smock.config.PerformanceConfig;
import com.smock.config.ResponseConfig;
import com.smock.models.CustomResponseConfig;
import com.smock.models.ResponsePath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomResponseMapper {

    @Autowired
    private PerformanceConfig performanceConfig;

    private Map<Integer, CustomResponseConfig> customResponseConfigMap = new HashMap<>();

    public void setCustomResponse(int apiId, CustomResponseConfig customResponseConfig) {
        customResponseConfigMap.put(apiId, customResponseConfig);
    }

    public CustomResponseConfig getCustomResponseConfig(int apiId) {
        return customResponseConfigMap.get(apiId);
    }

    public void clearCustom(int apiId) {
        customResponseConfigMap.remove(apiId);
    }

    public void clearAllCustom() {
        customResponseConfigMap.clear();
    }

}
