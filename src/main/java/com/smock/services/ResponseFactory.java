package com.smock.services;


import com.smock.config.ApiConfig;
import com.smock.config.PerformanceConfig;
import com.smock.models.ApiInfo;
import com.smock.models.ResponsePath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ResponseFactory {

    private static final Logger LOGGER = LogManager.getLogger(ResponseFactory.class);
    private Map<Integer, Map<Integer, String>> responseMap = new HashMap<>();
    private Map<Integer, String> defaultResponse = new HashMap<>();

    @Autowired private PerformanceConfig performanceConfig;

    @Value("${json.files.path}")
    private String jsonFilesPath;

    @PostConstruct
    public void init() {
        responseMap = new HashMap<>();
        List<ApiConfig> apiConfigs = performanceConfig.getApis();
        StringBuilder sb;
        for (ApiConfig apiConfig : apiConfigs) {
            Map<Integer, String> responses = new HashMap<>();
            for (ResponsePath responsePath : apiConfig.getResponse().getPaths()) {
                sb = new StringBuilder();
//            Resource resource = new ClassPathResource(apiConfig.getId() + ".json");
                FileReader fileReader = null;
                try {
                    fileReader = new FileReader(new File(jsonFilesPath + "/" + apiConfig.getName() + "/"
                            + responsePath.getPath() + ( (responsePath.getPath().endsWith(".json")) ? "" : ".json")));
                    try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        LOGGER.warn("Could not find response file for id " + apiConfig.getId() + " due to exception: ", e);
                        responseMap.put(apiConfig.getId(), null);
                    }
                    responses.put(responsePath.getId(), sb.toString());
                    if (responsePath.isDefaultPath()) {
                        defaultResponse.put(apiConfig.getId(), sb.toString());
                    }
                } catch (FileNotFoundException e) {
                    LOGGER.warn("Could not find response file for id " + apiConfig.getId() + " due to exception: ", e);
                    responseMap.put(apiConfig.getId(), null);
                }
            }
            responseMap.put(apiConfig.getId(), responses);
        }
    }

    public String getResponse(ApiInfo apiInfo) {
        return defaultResponse.get(apiInfo.getId());
    }

    public String getResponse(ApiInfo apiInfo, int id) {
        return responseMap.get(apiInfo.getId()).get(id);
    }

}
