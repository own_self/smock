package com.smock.models;

import java.util.Objects;

public class ApiInfo {

    private int id;
    private String name;
    private String url;
    private String urlPattern;
    private ApiMethod apiMethod;

    public ApiInfo() {
    }

    public ApiInfo(int id, String name, String url, String urlPattern, ApiMethod apiMethod) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.urlPattern = urlPattern;
        this.apiMethod = apiMethod;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public ApiMethod getApiMethod() {
        return apiMethod;
    }

    public void setApiMethod(ApiMethod apiMethod) {
        this.apiMethod = apiMethod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiInfo apiInfo = (ApiInfo) o;
        return id == apiInfo.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
