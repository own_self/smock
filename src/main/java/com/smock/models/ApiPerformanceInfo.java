package com.smock.models;

import java.util.concurrent.atomic.AtomicInteger;

public class ApiPerformanceInfo {

    private long latencyMisbehaveStart;
    private int latencyMisbehaveCount;

    public long getLatencyMisbehaveStart() {
        return latencyMisbehaveStart;
    }

    public void setLatencyMisbehaveStart(long latencyMisbehaveStart) {
        this.latencyMisbehaveStart = latencyMisbehaveStart;
    }

    public int getLatencyMisbehaveCount() {
        return latencyMisbehaveCount;
    }

    public void setLatencyMisbehaveCount(int latencyMisbehaveCount) {
        this.latencyMisbehaveCount = latencyMisbehaveCount;
    }
}
