package com.smock.config;

import com.smock.models.ResponsePath;

import java.util.List;

public class ResponseConfig {

    private String str;
    private List<ResponsePath> paths;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public List<ResponsePath> getPaths() {
        return paths;
    }

    public void setPaths(List<ResponsePath> paths) {
        this.paths = paths;
    }
}
