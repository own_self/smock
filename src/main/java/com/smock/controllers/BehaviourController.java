package com.smock.controllers;


import com.smock.models.*;
import com.smock.services.BehaviourInterruptManager;
import com.smock.services.CustomResponseMapper;
import com.smock.services.ValidationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/behaviour")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class BehaviourController {

    @Autowired
    private BehaviourInterruptManager behaviourInterruptManager;
    @Autowired
    private CustomResponseMapper customResponseMapper;
    @Autowired
    private ValidationService validationService;

    private static final Logger LOGGER = LogManager.getLogger(VulnTesterController.class);

    @RequestMapping()
    @ResponseBody
    public ResponseEntity behaviourInterrupt(@RequestParam("apiId") int apiId, @RequestParam("latency") int latency, @RequestParam("duration") long duration) {
        behaviourInterruptManager.interrupt(apiId, latency, duration);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/configure", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity configure(@RequestBody List<CustomResponseConfig> customResponseConfigList) {

        for(CustomResponseConfig customResponseConfig : customResponseConfigList) {
            customResponseMapper.setCustomResponse(customResponseConfig.getApiId(), customResponseConfig);
        }
        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/clearCustom/{apiId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity clearCustom(@PathVariable int apiId) {
        customResponseMapper.clearCustom(apiId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/clearAllCustom", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity clearAllCustom() {
        customResponseMapper.clearAllCustom();
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/validation", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addToValidationContext(@RequestBody List<ValidationContextDTO> validationContextDTOs) {
        validationContextDTOs.forEach(k -> validationService.addValidation(k));
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/validation/evaluate/{validationContextId}/{apiId}/{validationType}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ValidationResponse> validate(@PathVariable("validationContextId") int validationContextId
            , @PathVariable("apiId") int apiId, @PathVariable("validationType") ValidationType validationType) {
        ValidationResponse validate = validationService.validate(validationContextId, apiId, validationType);
        return new ResponseEntity<>(validate, HttpStatus.OK);
    }

}
