package com.smock.config;


import org.springframework.boot.context.properties.ConfigurationProperties;


public class MisbehaveConfig {

    private double latencyProbability;
    private int latencyDuration;
    private int latencyCount;
    private int latencyLow;
    private int latencyHigh;

    public MisbehaveConfig() {
    }

    public int getLatencyDuration() {
        return latencyDuration;
    }

    public void setLatencyDuration(int latencyDuration) {
        this.latencyDuration = latencyDuration;
    }

    public int getLatencyCount() {
        return latencyCount;
    }

    public void setLatencyCount(int latencyCount) {
        this.latencyCount = latencyCount;
    }

    public int getLatencyLow() {
        return latencyLow;
    }

    public void setLatencyLow(int latencyLow) {
        this.latencyLow = latencyLow;
    }

    public int getLatencyHigh() {
        return latencyHigh;
    }

    public void setLatencyHigh(int latencyHigh) {
        this.latencyHigh = latencyHigh;
    }

    public double getLatencyProbability() {
        return latencyProbability;
    }

    public void setLatencyProbability(double latencyProbability) {
        this.latencyProbability = latencyProbability;
    }
}
