package com.smock.services;


import com.smock.models.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ValidationService {

    @Autowired
    private ValidationContextHolder validationContextHolder;

    private static final Logger LOGGER = LogManager.getLogger(ValidationService.class);

    public void addValidation(ValidationContextDTO validationContextDTO) {
        Optional<ValidationContext> validationContextOptional = validationContextHolder.getValidationContext(validationContextDTO.getValidationContextId());
        ValidationContext validationContext = validationContextOptional.orElseGet(ValidationContext::new);
        validationContextHolder.addExpectedContext(validationContextDTO.getValidationContextId(), validationContext);
        int apiId = validationContextDTO.getApiId();
        ValidationType validationType = validationContextDTO.getValidationType();
        Map<ValidationType, Map<String, Object>> validationTypeMap = validationContext.getContext().computeIfAbsent(apiId, k -> new HashMap<>());
        Map<String, Object> validationEntity = validationTypeMap.computeIfAbsent(validationType, k -> new HashMap<>());
        for(ValidationEntity requestEntity : validationContextDTO.getValidationEntities()) {
            validationEntity.put(requestEntity.getKey(), requestEntity.getValue());
        }
        LOGGER.info("Validation updated.");
    }

    public ValidationResponse validate(int validationContextId, int apiId, ValidationType validationType) {
        Optional<Map<ValidationType, Map<String, Object>>> validationContextForApiIdOptional = validationContextHolder.getValidationContextForApiId(validationContextId, apiId);
        ValidationResponse validationResponse = new ValidationResponse();
        List<ValidationFailureEntity> validationFailureEntityList = new ArrayList<>();
        validationResponse.setValidationContextId(validationContextId);
        validationResponse.setApiId(apiId);
        validationResponse.setValid(true);
        validationResponse.setValidationFailureEntities(validationFailureEntityList);
        if(validationContextForApiIdOptional.isPresent()) {
            Map<ValidationType, Map<String, Object>> validationTypeMap = validationContextForApiIdOptional.get();
            Map<String, Object> validationEntities = validationTypeMap.get(validationType);
            if(validationEntities != null) {
                Optional<ValidationContext> observedContextOptional = validationContextHolder.getObservedContext(apiId);
                if(!observedContextOptional.isPresent()) {
                    return validationResponse;
                }
                ValidationContext observedValidationContext = observedContextOptional.get();
                Map<ValidationType, Map<String, Object>> observedContext = observedValidationContext.getObservedContext();
                if(observedContext == null || observedContext.get(validationType) == null) {
                    return validationResponse;
                }

                Map<String, Object> observedValues = observedContext.get(validationType);

                for(Map.Entry<String, Object> expectedEntry : validationEntities.entrySet()) {
                    Object expectedValue = expectedEntry.getValue();
                    Object observedValue = observedValues.get(expectedEntry.getKey());
                    if(expectedValue == null && observedValue == null) {
                        continue;
                    }
                    if(observedValue != null && !observedValue.equals(expectedValue)) {
                        ValidationFailureEntity validationFailureEntity = new ValidationFailureEntity();
                        validationFailureEntity.setValidationType(validationType);
                        validationFailureEntity.setKey(expectedEntry.getKey());
                        validationFailureEntity.setExpectedValue(expectedValue);
                        validationFailureEntity.setObservedValue(observedValue);
                        validationFailureEntityList.add(validationFailureEntity);
                        validationResponse.setValid(false);
                    }
                }
//
//                for(Map.Entry<String, Object> entry : expectedValues.entrySet()) {
//                    Object expectedValue = validationEntities.get(entry.getKey());
//                    if(expectedValue != null && !expectedValue.equals(entry.getValue())) {
//                        ValidationFailureEntity validationFailureEntity = new ValidationFailureEntity();
//                        validationFailureEntity.setValidationType(validationType);
//                        validationFailureEntity.setKey(entry.getKey());
//                        validationFailureEntity.setExpectedValue(expectedValue);
//                        validationFailureEntity.setObservedValue(entry.getValue());
//                        validationFailureEntityList.add(validationFailureEntity);
//                        validationResponse.setValid(false);
//                    }
//                }
            }
        }
        return validationResponse;
    }

    public void updateObserved(int apiId, ValidationType validationType, Map<String, Object> values) {
        Optional<ValidationContext> observedContextOptional = validationContextHolder.getObservedContext(apiId);
        ValidationContext validationContext = observedContextOptional.orElseGet(ValidationContext::new);
        Map<String, Object> observedSoFar = validationContext.getObservedContext().computeIfAbsent(validationType, k -> new HashMap<>());
        observedSoFar.putAll(values);
        validationContextHolder.removeObservedContext(apiId);
        validationContextHolder.addObservedContext(apiId, validationContext);
    }

}
